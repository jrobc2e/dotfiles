#!/bin/sh

if [ $1 == "Doublecmd" ]; then
	id=$(printf '0x%x\n' $(xdotool search --name "Double Commander 1.0.0"))
else
	id=$(xdo id -n $1);
fi
if [ -z "$id" ] || [ "$id" == "0x0" ]; then
	if [ $1 == "scratchemacs" ]; then
		emacsclient -c --alternate-editor='' --frame-parameters='(quote (name . "scratchemacs"))'
	elif [ $1 == "Doublecmd" ]; then
		dcmd
	fi
else
	action='hide';
	if [[ $(xprop -id $id | awk '/window state: / {print $3}') == 'Withdrawn' ]]; then
		action='show';
	fi
	if [ $1 == "Doublecmd" ]; then
		xdo $action $id
	else
		xdo $action -n $1
	fi
fi
