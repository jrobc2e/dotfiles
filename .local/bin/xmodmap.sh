#!/bin/sh
/home/jrobc2e/.local/bin/disable-xmodmap.sh
pkill xcape

spare_modifier="Hyper_L"
xmodmap -e "keycode 23 = $spare_modifier"
#xmodmap -e "keycode 51 = Alt_R"

xmodmap -e "keycode any = Tab"
xcape -e "$spare_modifier=Tab"
