;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'unused

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '(vimscript
	 ;; ----------------------------------------------------------------
	 ;; Example of useful layers you may want to use right away.
	 ;; Uncomment some layer names and press <SPC f e R> (Vim style) or
	 ;; <M-m f e R> (Emacs style) to install them.
	 ;; ----------------------------------------------------------------
	 helm
	 (auto-completion
	  ;; :variables auto-completion-enable-snippets-in-popup t
	  :disabled-for org)
	 ;; better-defaults
	 c-c++
	 colors
	 csv
	 cscope
	 emacs-lisp
	 erc
	 (ess :variables
		  ess-assign-key "\M--")
	 (evil-snipe :variables
				 evil-snipe-enable-alternate-f-and-t-behaviors t)
	 fasd
	 games
	 git
	 html
	 javascript
	 latex
	 markdown
	 multiple-cursors
	 ;; neotree
	 (org :variables
		  org-want-todo-bindings t
		  org-use-speed-commands t
		  ;; org-enable-sticky-header t
		  )
	 ;; pdf-tools
	 python
	 ranger
	 (shell :variables
			shell-default-height 30
			shell-default-position 'bottom)
	 spacemacs-layouts
	 speed-reading
	 ;; spell-checking
	 syntax-checking
	 systemd
	 themes-megapack
	 treemacs
	 version-control
	 (c-c++ :variables
			c-c++-enable-clang-support t)
	 semantic
	 )

   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   dotspacemacs-additional-packages '(anki-editor
									  irony
									  vimish-fold
									  evil-vimish-fold
									  ;; smooth-scroll
									  company-irony
									  company-irony-c-headers
									  auctex
									  cdlatex
									  company-auctex
									  org-gcal
									  ox-pandoc
									  )

   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '(smooth-scrolling)

   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))
(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; Name of executable file pointing to emacs 27+. This executable must be
   ;; in your PATH.
   ;; (default "emacs")
   dotspacemacs-emacs-pdumper-executable-file "emacs"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=~/.emacs.d/.cache/dumps/spacemacs.pdmp
   ;; (default spacemacs.pdmp)
   dotspacemacs-emacs-dumper-dump-file "spacemacs.pdmp"

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default t)
   dotspacemacs-verify-spacelpa-archives t

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style '(vim :variables
                                    vim-style-remap-Y-to-y$ t)

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Default major mode for a new empty buffer. Possible values are mode
   ;; names such as `text-mode'; and `nil' to use Fundamental mode.
   ;; (default `text-mode')
   dotspacemacs-new-empty-buffer-major-mode 'text-mode

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(
                         doom-vibrant
						 ;; doom-dracula
                         doom-molokai
						 doom-outrun-electric
						 ;; doom-moonlight
						 ;; doom-snazzy
                         ;; omtose-darker
                         ;; monokai
                         ;; zenburn
                         ;; birds-of-paradise-plus
                         ;; bubbleberry
                         ;; tango
                         ;; whiteboard
                         ;; soft-morning
                         ;; gruvbox-light-soft
                         ;; twilight-bright
						 doom-nord-light
						 doom-solarized-light
                         )

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   ;; dotspacemacs-mode-line-theme '(spacemacs :separator box :separator-scale 1.3)
   dotspacemacs-mode-line-theme 'doom

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font or prioritized list of fonts.
   ;; dotspacemacs-default-font '("Source Code Pro"
   ;;                             :size 10.0
   ;;                             :weight normal
   ;;                             :width normal)

   ;; dotspacemacs-default-font '("Consolas"
   ;;                             :size 16
   ;;                             :weight Normal
   ;;                             :width Normal
   ;;                             :powerline-scale 1.5
   ;;                             )

   dotspacemacs-default-font '("IosevkaBest"
   							   :size 12
   							   ;; :weight Medium
   							   :width Normal
   							   :powerline-scale 1.2
   							   )

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key ":"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab t

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts t

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar nil

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; If non-nil the frame is undecorated when Emacs starts up. Combine this
   ;; variable with `dotspacemacs-maximized-at-startup' in OSX to obtain
   ;; borderless fullscreen. (default nil)
   dotspacemacs-undecorated-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols nil

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t', `relative' or `visual' then line numbers are enabled in all
   ;; `prog-mode' and `text-mode' derivatives. If set to `relative', line
   ;; numbers are relative. If set to `visual', line numbers are also relative,
   ;; but lines are only visual lines are counted. For example, folded lines
   ;; will not be counted and wrapped lines are counted as multiple lines.
   ;; This variable can also be set to a property list for finer control:
   dotspacemacs-line-numbers nil
   ;; '(:relative t
   ;; 			   :visual nil
   ;; 			   :disabled-for-modes dired-mode
   ;; 			   doc-view-mode
   ;; 			   markdown-mode
   ;; 			   org-mode
   ;; 			   pdf-view-mode
   ;; 			   text-mode
   ;; 			   :size-limit-kb 1000)
   ;; When used in a plist, `visual' takes precedence over `relative'.
   ;; (default nil)

   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil
   ;; dotspacemacs-folding-method 'origami

   ;; If non-nil `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc...
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server t
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup nil

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil))
(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."

  (setq evil-snipe-use-vim-sneak-bindings t)
  (setq reftex-extra-bindings t)
  (defun my-latex ()
    (spacemacs/toggle-auto-fill-mode-off)
  	;; (visual-line-mode 1)
    (spacemacs/toggle-visual-line-navigation-on))
  (add-hook 'LaTeX-mode-hook 'my-latex)


  )

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."

                                        ;_UI
  (set-face-attribute 'default nil
					  :family "IosevkaBest"
					  :height 120
					  ;; :weight 'normal
					  :width 'normal)
  ;; (setq doom-vibrant-brighter-modeline t)
  ;; (setq doom-modeline-minor-modes (featurep 'minions))
  ;; (setq doom-modeline-height 20)
  ;; (setq doom-modeline-icon (display-graphic-p))
  (setq doom-modeline-buffer-encoding nil)
  ;; (setq doom-modeline)
  (setq zone-programs [zone-pgm-whack-chars]) ; Best effect - make this the only one that's chosen
  (setq-default ranger-show-literal nil)
  (setq evil-ex-search-persistent-highlight nil) ; Keep typing to make unique match; this prevents the persistent highlight that comes from pressing n to get the next match
  ;; (setq evil-ex-search-highlight-all nil)
  ;; (setq evil-ex-search-interactive nil) ; interactive means it displays all matches like vim after hitting n
  (setq scroll-conservatively 101) ;; move minimum when cursor exits view, instead of recentering
  (setq mouse-wheel-scroll-amount '(3)) ;; mouse scroll moves 1 line at a time, instead of 5 lines
  (setq mouse-wheel-progressive-speed nil) ;; on a long mouse scroll keep scrolling by 1 line
  ;; (set-face-attribute 'default nil :family "Consolas")
  ;; (spacemacs/toggle-display-time-on)
  ;; (setq display-time-24hr-format t)
  ;; (spacemacs/toggle-mode-line-point-position-on) ; shows character number, is superfluous
  ;; (spaceline-toggle-buffer-encoding-abbrev-off) ; RECENT_CHANGE
  (spacemacs/toggle-mode-line-battery-on)
  (spacemacs/toggle-mode-line)
  ;; (setq powerline-default-separator 'arrow-fade)
  ;; (setq powerline-default-separator 'box)
  (setq lazy-highlight-cleanup t)
  ;; (setq fringe-mode '(1 . 1))
										;_indentation
  (global-aggressive-indent-mode 1)
  (setq-default indent-tabs-mode t)
  (setq-default tab-width 4)
  (setq-default c-basic-offset 4)
  (setq c-default-style "linux")
  (setq python-indent-guess-indent-offset t)
										;_interaction
  (defun then_R_operator ()
	"R - %>% operator or 'then' pipe operator"
	(interactive)
	(just-one-space 1)
	(insert "%>%")
	(reindent-then-newline-and-indent))
  (evil-define-key 'insert ess-mode-map (kbd "C-S-M") 'then_R_operator)
  (evil-define-key 'insert inferior-ess-mode-map (kbd "C-S-M") 'then_R_operator)

  (vimish-fold-global-mode 1)
  (evil-vimish-fold-mode 1)
  (global-company-mode)
  (company-tng-configure-default)
  (defun kill-back-to-indentation ()
	"Kill from point back to the first non-whitespace character on the line."
	(interactive)
	(let ((prev-pos (point)))
	  (back-to-indentation)
	  (kill-region (point) prev-pos)))
  (global-set-key (kbd "C-M-<backspace>") 'kill-back-to-indentation)

  (defun my-ex-replacement (&optional arg)
	(interactive "P")
	(let ((evil-ex-substitute-global t))
	  (cond ((eq arg 2) (evil-ex "'<,'>s/\\, /,\\n/"))
			((eq arg 3) (evil-ex "'<,'>s/^[[:space:]]*\n//"))
			((eq arg 4) (evil-ex "'<,'>s/^[[:space:]]*\\(•\\|\\|\\|◼\\)\\n*[[:space:]]*/- /"))
			((eq arg 5) (evil-ex "'<,'>s/-?\\n\\([^-] *\\)/ \\1"))
			((eq arg 6) (evil-ex "'<,'>s/“\\|”/\"/"))
			((eq arg 7) (evil-ex "'<,'>s/‘\\|’/'/"))
			(t (evil-ex "'<,'>s/\\(\\.\\|?\\|!\\) /\\1\\n/")))
	  ))

  (setq-default evil-jumps-cross-buffers nil)
  (define-key evil-insert-state-map (kbd "C-u") (lambda () (evil-delete-line)))
  (setq-default evil-ex-substitute-global t)
  (unbind-key (kbd "-") evil-normal-state-map) ; disables the deer shortcut overriding vim
  (setq ranger-cleanup-eagerly t)
  (setq avy-timeout-seconds 0.3)
  (setq persp-set-read-buffer-function t)
  (setq python-indent-guess-indent-offset-verbose nil)
  (setq auto-completion-enable-help-tooltip 'manual)
										;_LaTeX
  (require 'company-auctex)
  (setq latex-build-command "LaTeX")
  (setq TeX-save-query nil)

  (setq reftex-plug-into-AUCTeX t)
  (setq reftex-use-external-file-finders t)
  (setq reftex-external-file-finders
		'(("tex" . "/usr/bin/kpsewhich -format=.tex %f")
		  ("bib" . "/usr/bin/kpsewhich -format=.bib %f")))
  (defcustom TeX-buf-close-at-warnings-only t
	"Close TeX buffer if there are only warnings."
	:group 'TeX-output
	:type 'boolean)

  (defun my-tex-close-TeX-buffer (_output)
	"Close compilation buffer if there are no errors.
Hook this function into `TeX-after-compilation-finished-functions'."
	(let ((buf (TeX-active-buffer)))
	  (when (buffer-live-p buf)
		(with-current-buffer buf
		  (when (progn (TeX-parse-all-errors)
					   (or
						(and TeX-buf-close-at-warnings-only
							 (null (cl-assoc 'error TeX-error-list)))
						(null TeX-error-list)))
			(cl-loop for win in (window-list)
					 if (eq (window-buffer win) (current-buffer))
					 do (delete-window win)))))))

  (add-hook 'TeX-after-compilation-finished-functions #'my-tex-close-TeX-buffer)
  ;; (defun TeXpreview_auto ()
  ;; 	(when (looking-back (rx "$ "))
  ;; 	  (save-excursion
  ;; 		(backward-char 1)
  ;; 		(org-toggle-latex-fragment))))
                                        ;_org
  (setq org-agenda-show-future-repeats nil)
  (setq org-file-apps ;open export with firefox
		(quote
		 ((auto-mode . emacs)
		  ("\\.mm\\'" . default)
		  ("\\.x?html?\\'" . "/usr/bin/firefox %s")
		  ("\\.pdf\\'" . "/usr/bin/okular %s"))))
  (setq org-list-demote-modify-bullet '(("+" . "-") ("-" . "+") ("*" . "+"))) ;
  (setq evil-org-key-theme
  		'(navigation insert return textobjects additional shift todo heading calendar))
  ;; (evil-org-set-key-theme '(textobjects insert navigation additional shift todo heading))
  (setq org-special-ctrl-a/e t)
  (setq org-special-ctrl-k t)
  (setq org-blank-before-new-entry '((heading . nil) (plain-list-item . nil)))
  (setq org-M-RET-may-split-line '((default . nil)))
  (setq org-download-heading-lvl nil)
  (setq org-startup-with-latex-preview t)
  (setq org-startup-with-inline-images t)
  (require 'org-tempo) ;; #+... completion
  (setq org-refile-use-outline-path 'file)
  (setq org-outline-path-complete-in-steps nil)
  (setq org-refile-allow-creating-parent-nodes 'confirm)
  (setq org-agenda-files
		'("/home/jrobc2e/Dropbox/Org/Agenda.org"))
  (setq org-agenda-start-on-weekday nil)
  (setq org-refile-targets
		'((nil :maxlevel . 2)
		  (org-agenda-files :maxlevel . 1)))
  (setq org-todo-keywords
		'((sequence "TODO(t)" "IDEA(i)" "|" "DONE(d)")))
  (setq org-download-image-dir "/home/jrobc2e/Dropbox/School/.org/.orgDownloads/")
  (setq org-download-annotate-function (lambda (_) ""))
  (setq org-download-screenshot-method
		"xclip -selection clipboard -t image/png -o > %s")
  (setq org-image-actual-width nil)
  (setq org-latex-image-default-width "")
  ;; (setq org-download-image-org-width -1)
  (setq org-pretty-entities t)
  (setq org-pretty-entities-include-sub-superscripts t)
  (setq org-use-sub-superscripts "{}")
  (setq org-hide-emphasis-markers t)
  ;; (setq org-inline-image-overlays t)
  (setq org-bullets-bullet-list '("■" "◆" "▲" "▶"))
  ;; (setq org-bullets-bullet-list '("⊢" "⋱" "⇁" "○" "•"))
  ;; (setq org-ellipsis " ⤵")
  (setq org-ellipsis " ▼")
  (setq org-sticky-header-full-path t)
  (setq org-fontify-whole-heading-line t)
  (setq org-fontify-done-headline t)
  (setq org-fontify-emphasized-text t)
  (setq org-fontify-quote-and-verse-blocks t)
  (setq org-hide-leading-stars t)
  (evil-define-key 'normal evil-org-mode-map
	(kbd "M-[") 'org-backward-element
	(kbd "M-]") 'org-forward-element
	(kbd "O") (lambda () (interactive) (evil-insert-state) (org-insert-heading-after-current)))

  (use-package org-gcal
	:ensure t
	:config
	(setq org-gcal-client-id "637462050412-lj6fumt9sv3gd9q130v2j6qgob891pn4.apps.googleusercontent.com"
		  org-gcal-client-secret "6qrcjv9X-DE39CO1PqlUWKtg"
		  org-gcal-file-alist '(("jrobc2e@gmail.com" .  "~/Dropbox/Org/cal.org"))))

  (defun myExp (arg)
	(interactive "p")
	(insert (format "\\times10^{%d}" arg))
	)

  (defun mySizeLabel (arg)
	(interactive "P")
	(beginning-of-line)
	(if (> arg 20)
		(progn
		  (insert (format "#+ATTR_ORG: :width %spx\n" arg))
		  (insert (format "#+ATTR_LATEX: :width %spx\n" (round (* arg 0.64))))
		  (org-display-inline-images))
	  (progn
		(insert (format "#+ATTR_ORG: :scale %.2f\n" (/ 1.0 arg)))
		(insert (format "#+ATTR_LATEX: :scale %.2f\n" (/ 1.0 arg)))
		(org-display-inline-images))
	  )
	)

  (defun myscreenshot (arg)
	(interactive "P")
	(beginning-of-line)

	(let ((beg (point))))
	(org-download-screenshot)
	(forward-line -2)
	(kill-line)
	(when arg
	  (mySizeLabel arg))
	;; (open-line 1)
	)
                                        ;_hooks
  (add-hook 'pdf-tools-enabled-hook 'pdf-annot-minor-mode '(linum-mode nil))
  (add-hook 'c++-mode-hook
            (lambda ()
              (unless (or (file-exists-p "makefile")
                          (file-exists-p "Makefile"))
                (set (make-local-variable 'compile-command)
                     (concat "g++ -o " (file-name-directory buffer-file-name) "bin/"
                             (file-name-sans-extension (file-name-nondirectory buffer-file-name))" "
                             (buffer-file-name))))))

  (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
        TeX-source-correlate-start-server t)
  (add-hook 'doc-view-mode-hook 'auto-revert-mode)

  (add-hook 'org-mode-hook
			(lambda ()
			  ;; (define-key evil-operator-state-map (kbd "<remap> <evil-next-line>")     #'evil-next-line)
			  ;; (define-key evil-operator-state-map (kbd "<remap> <evil-previous-line>") #'evil-previous-line)
			  (spacemacs/toggle-visual-line-navigation-on)
			  (org-cdlatex-mode)
			  (org-bullets-mode)
			  (org-indent-mode)
			  ;; (setq vi-tilde-fringe-mode nil) ;not working
			  ;; (spacemacs/set-leader-keys-for-major-mode 'org-mode ":" 'org-set-tags-command)
			  (spacemacs/set-leader-keys-for-major-mode 'org-mode "<M-return>" 'org-insert-heading-after-current)
			  (sp-local-pair 'org-mode "*" nil :actions :rem)
			  (sp-local-pair 'org-mode "_" nil :actions :rem)
			  (sp-local-pair 'org-mode "/" nil :actions :rem)
			  (sp-local-pair 'org-mode "~" nil :actions :rem)
			  (sp-local-pair 'org-mode "=" nil :actions :rem)
			  (sp-local-pair 'org-mode "\\" nil :actions :rem)
			  (sp-local-pair 'org-mode "\\[" "\\]")
			  ;; (smartparens-mode)
			  (spacemacs/toggle-smartparens-on) ; try this; line above doesn't seem to persist
			  ;; (with-eval-after-load 'org
			  ;; 	(define-key  (kbd "<C-return>") 'org-insert-heading-respect-content))
			  ;; (add-hook 'post-self-insert-hook #'TeXpreview_auto 'append 'local)
			  ;; (lambda ()
			  ;;   (setq-local yas/trigger-key "C-'")
			  ;;   (define-key yas/keymap [tab] 'yas/next-field-or-maybe-expand)))
			  ))
  (define-key yas-minor-mode-map (kbd "C-'") #'yas-expand)

  ;; (with-eval-after-load 'org-mode
  ;; 	(evil-define-key '(normal insert) org-mode-map (kbd "<C-return>") 'org-insert-heading-respect-content)
  ;; 	(evil-define-key '(normal insert) org-mode-map (kbd "<C-M-return>") 'evil-org-org-insert-heading-respect-content-below)
  ;; 	)
                                        ;_binds
  (spacemacs/set-leader-keys "o'" 'mySizeLabel)
  (spacemacs/set-leader-keys "o0" 'myExp)
  (spacemacs/set-leader-keys "ot" (lambda () (interactive) (org-capture nil "t")))
  (spacemacs/set-leader-keys "os" (lambda () (interactive) (org-capture nil "s")))
  (spacemacs/set-leader-keys "ok" (lambda () (interactive) (save-buffer) (org-latex-export-to-pdf)))
  (spacemacs/set-leader-keys "oj" 'evil-join)
  (spacemacs/set-leader-keys "on" 'org-toggle-pretty-entities)
  (spacemacs/set-leader-keys "oN" 'org-toggle-link-display)
  (spacemacs/set-leader-keys "wa" 'delete-other-windows)
  (spacemacs/set-leader-keys "ov" 'visual-line-mode)
  (spacemacs/set-leader-keys "op" 'persp-mode)
  (spacemacs/set-leader-keys "oi" 'org-toggle-latex-fragment)
  (spacemacs/set-leader-keys "ol" 'org-toggle-latex-fragment)
  (spacemacs/set-leader-keys "oI" 'org-display-inline-images)
  (spacemacs/set-leader-keys "SPC" 'lazy-helm/helm-mini)
  (spacemacs/set-leader-keys "o;" 'myscreenshot)
  (spacemacs/set-leader-keys "oo" 'my-ex-replacement)
  (spacemacs/set-leader-keys "ou" 'org-toggle-inline-images)
  (global-set-key (kbd "C-;") 'avy-goto-char-timer)

  (setq org-capture-templates
		'(
		  ("t" "Todo" entry (file "/home/jrobc2e/Dropbox/Org/Agenda"))))

  )
										;_company
(with-eval-after-load 'company
  '(lambda() (setq spacemacs-default-company-backends (delete 'company-dabbrev spacemacs-default-company-backends)))
  (setq company-backends '((company-capf company-keywords company-yasnippet company-files company-dabbrev-code company-etags company-dabbrev)))
  (company-auctex-init)
  (setq company-dabbrev-other-buffers nil) ;; don't flood buffer with random shit from other buffers
  (setq company-dabbrev-ignore-case t)

  ;; (setq company-dabbrev-char-regexp "\\S-") ;; \\S means 'not', - means 'whitespace; allows for odd characters & brackets in words matched
  (setq company-dabbrev-char-regexp "[\\[:word:]-_^{+}]") ;; more sane than the above, which matches chars like /:' etc.

  ;; (setq company-dabbrev-downcase 0)
  (setq company-idle-delay 0)
  ;; (setq company-transformers '(company-sort-by-occurrence))
  )

;; Do not write anything past this comment. This is where Emacs will
;; auto-generate custom variable definitions.
(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
  (custom-set-variables
   ;; custom-set-variables was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(TeX-source-correlate-start-server t)
   '(TeX-view-program-selection
	 (quote
	  ((output-pdf "Okular")
	   ((output-dvi has-no-display-manager)
		"dvi2tty")
	   ((output-dvi style-pstricks)
		"dvips and gv")
	   (output-dvi "xdvi")
	   (output-pdf "Evince")
	   (output-html "xdg-open"))))
   '(ansi-term-color-vector
	 [unspecified "#FFFFFF" "#d15120" "#5f9411" "#d2ad00" "#6b82a7" "#a66bab" "#6b82a7" "#505050"])
   '(column-number-mode t)
   '(company-quickhelp-color-background "#b0b0b0")
   '(company-quickhelp-color-foreground "#232333")
   '(compilation-message-face (quote default))
   '(custom-safe-themes
	 (quote
	  ("f951343d4bbe5a90dba0f058de8317ca58a6822faa65d8463b0e751a07ec887c" default)))
   '(fci-rule-character-color "#d9d9d9")
   '(fci-rule-color "#c7c7c7")
   '(flycheck-pylint-use-symbolic-id nil)
   '(highlight-changes-colors (quote ("#FD5FF0" "#AE81FF")))
   '(highlight-tail-colors
	 (quote
	  (("#3C3D37" . 0)
	   ("#679A01" . 20)
	   ("#4BBEAE" . 30)
	   ("#1DB4D0" . 50)
	   ("#9A8F21" . 60)
	   ("#A75B00" . 70)
	   ("#F309DF" . 85)
	   ("#3C3D37" . 100))))
   '(magit-diff-use-overlays nil)
   '(nrepl-message-colors
	 (quote
	  ("#336c6c" "#205070" "#0f2050" "#806080" "#401440" "#6c1f1c" "#6b400c" "#23733c")))
   '(org-calc-default-modes
	 (quote
	  (calc-internal-prec 12 calc-float-format
						  (sci 4)
						  calc-angle-mode deg calc-prefer-frac nil calc-symbolic-mode nil calc-date-format
						  (YYYY "-" MM "-" DD " " Www
								(" " hh ":" mm))
						  calc-display-working-message t)))
   '(org-export-async-init-file "/home/jrobc2e/Dropbox/Org/org-async-init.el" t)
   '(org-format-latex-header
	 "\\documentclass{article}
   \\usepackage[usenames]{color}
   [PACKAGES]
   [DEFAULT-PACKAGES]
   \\pagestyle{empty}             % do not remove
   % The settings below are copied from fullpage.sty
   \\setlength{\\textwidth}{\\paperwidth}
   \\addtolength{\\textwidth}{-3cm}
   \\setlength{\\oddsidemargin}{1.5cm}
   \\addtolength{\\oddsidemargin}{-2.54cm}
   \\setlength{\\evensidemargin}{\\oddsidemargin}
   \\setlength{\\textheight}{\\paperheight}
   \\addtolength{\\textheight}{-\\headheight}
   \\addtolength{\\textheight}{-\\headsep}
   \\addtolength{\\textheight}{-\\footskip}
   \\addtolength{\\textheight}{-3cm}
   \\setlength{\\topmargin}{1.5cm}
   \\addtolength{\\topmargin}{-2.54cm}\\n\\setlength{\\parindent}{0pt}")
   '(org-format-latex-options
	 (quote
	  (:foreground default :background default :scale 1.4 :html-foreground "Black" :html-background "Transparent" :html-scale 1.0 :matchers
				   ("begin" "$1" "$" "$$" "\\(" "\\["))))
   '(org-latex-packages-alist nil)
   '(org-preview-latex-default-process (quote imagemagick))
   '(org-todo-keyword-faces (quote (("IDEA" . "#a3d6ff"))))
   '(package-selected-packages
	 (quote
	  (fasd smooth-scroll csv-mode org-caldav neotree company-quickhelp zenburn-theme zen-and-art-theme yasnippet-snippets yapfify xterm-color ws-butler writeroom-mode winum white-sand-theme which-key web-mode web-beautify volatile-highlights vi-tilde-fringe uuidgen use-package underwater-theme ujelly-theme typit twilight-theme twilight-bright-theme twilight-anti-bright-theme treemacs-projectile treemacs-evil toxi-theme toc-org tao-theme tangotango-theme tango-plus-theme tango-2-theme tagedit systemd symon symbol-overlay sunny-day-theme sudoku sublime-themes subatomic256-theme subatomic-theme string-inflection stickyfunc-enhance srefactor spray spaceline-all-the-icons spacegray-theme soothe-theme solarized-theme soft-stone-theme soft-morning-theme soft-charcoal-theme smyx-theme smeargle slim-mode shell-pop seti-theme scss-mode sass-mode reverse-theme restart-emacs rebecca-theme ranger rainbow-mode rainbow-identifiers rainbow-delimiters railscasts-theme pytest pyenv-mode py-isort purple-haze-theme pug-mode professional-theme prettier-js popwin planet-theme pippel pipenv pip-requirements phoenix-dark-pink-theme phoenix-dark-mono-theme persp-mode pcre2el password-generator paradox pacmacs ox-pandoc overseer orgit organic-green-theme org-projectile org-present org-pomodoro org-mime org-gcal org-download org-cliplink org-bullets org-brain open-junk-file omtose-phellack-theme oldlace-theme occidental-theme obsidian-theme nodejs-repl noctilux-theme naquadah-theme nameless mustang-theme multi-term move-text monokai-theme monochrome-theme molokai-theme moe-theme mmm-mode minimal-theme material-theme markdown-toc majapahit-theme magit-svn magit-gitflow madhat2r-theme macrostep lush-theme lorem-ipsum livid-mode live-py-mode link-hint light-soap-theme kaolin-themes json-navigator json-mode js2-refactor js-doc jbeans-theme jazz-theme ir-black-theme inkpot-theme indent-guide importmagic impatient-mode hungry-delete hl-todo highlight-parentheses highlight-numbers highlight-indentation heroku-theme hemisu-theme helm-xref helm-themes helm-swoop helm-rtags helm-pydoc helm-purpose helm-projectile helm-org-rifle helm-org helm-mode-manager helm-make helm-gitignore helm-git-grep helm-flx helm-descbinds helm-css-scss helm-cscope helm-company helm-c-yasnippet helm-ag hc-zenburn-theme gruvbox-theme gruber-darker-theme grandshell-theme gotham-theme google-translate google-c-style golden-ratio gnuplot gitignore-templates gitconfig-mode gitattributes-mode git-timemachine git-messenger git-link git-gutter-fringe git-gutter-fringe+ gh-md gandalf-theme fuzzy font-lock+ flycheck-rtags flycheck-pos-tip flycheck-package flx-ido flatui-theme flatland-theme fill-column-indicator farmhouse-theme fancy-battery eziam-theme eyebrowse expand-region exotica-theme evil-visualstar evil-visual-mark-mode evil-unimpaired evil-tutor evil-textobj-line evil-surround evil-snipe evil-org evil-numbers evil-nerd-commenter evil-mc evil-matchit evil-magit evil-lisp-state evil-lion evil-indent-plus evil-iedit-state evil-goggles evil-exchange evil-escape evil-ediff evil-cleverparens evil-args evil-anzu eval-sexp-fu espresso-theme eshell-z eshell-prompt-extras esh-help erc-yt erc-view-log erc-social-graph erc-image erc-hl-nicks emmet-mode elisp-slime-nav editorconfig dumb-jump dracula-theme dotenv-mode doom-themes doom-modeline django-theme disaster diminish diff-hl devdocs define-word darktooth-theme darkokai-theme darkmine-theme darkburn-theme dakrone-theme cython-mode cyberpunk-theme cpp-auto-include company-web company-tern company-statistics company-rtags company-reftex company-irony-c-headers company-irony company-c-headers company-auctex company-anaconda column-enforce-mode color-theme-sanityinc-tomorrow color-theme-sanityinc-solarized color-identifiers-mode clues-theme clean-aindent-mode clang-format chocolate-theme cherry-blossom-theme centered-cursor-mode cdlatex busybee-theme bubbleberry-theme browse-at-remote blacken birds-of-paradise-plus-theme badwolf-theme auto-yasnippet auto-highlight-symbol auto-compile auctex-latexmk apropospriate-theme anti-zenburn-theme ample-zen-theme ample-theme alect-themes aggressive-indent afternoon-theme ace-link ace-jump-helm-line ac-ispell 2048-game)))
   '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
   '(persp-mode t nil (persp-mode))
   '(pos-tip-background-color "#FFFACE")
   '(pos-tip-foreground-color "#272822")
   '(reftex-bibpath-environment-variables
	 (quote
	  ("BIBINPUTS" "TEXBIB" "!kpsewhich -show-path=.bib" "\"/home/jrobc2e/texmf/EcolLab\"")))
   '(tool-bar-mode nil)
   '(vc-annotate-background "#d4d4d4")
   '(vc-annotate-color-map
	 (quote
	  ((20 . "#437c7c")
	   (40 . "#336c6c")
	   (60 . "#205070")
	   (80 . "#2f4070")
	   (100 . "#1f3060")
	   (120 . "#0f2050")
	   (140 . "#a080a0")
	   (160 . "#806080")
	   (180 . "#704d70")
	   (200 . "#603a60")
	   (220 . "#502750")
	   (240 . "#401440")
	   (260 . "#6c1f1c")
	   (280 . "#935f5c")
	   (300 . "#834744")
	   (320 . "#732f2c")
	   (340 . "#6b400c")
	   (360 . "#23733c"))))
   '(vc-annotate-very-old-color "#23733c")
   '(weechat-color-list
	 (quote
	  (unspecified "#272822" "#3C3D37" "#F70057" "#F92672" "#86C30D" "#A6E22E" "#BEB244" "#E6DB74" "#40CAE4" "#66D9EF" "#FB35EA" "#FD5FF0" "#74DBCD" "#A1EFE4" "#F8F8F2" "#F8F8F0"))))
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   '(default ((t (:background nil))))
   '(org-ellipsis ((t (:foreground "#B2DEC1" :underline nil))))
   '(org-level-1 ((t (:inherit outline-1 :height 1.3))))
   '(org-level-2 ((t (:inherit outline-2 :height 1.2))))
   '(org-level-3 ((t (:inherit outline-3 :height 1.15))))
   '(org-level-4 ((t (:inherit outline-4 :height 1.1))))
   '(rainbow-delimiters-depth-1-face ((t (:foreground "dark orange"))))
   '(rainbow-delimiters-depth-2-face ((t (:foreground "deep pink"))))
   '(rainbow-delimiters-depth-3-face ((t (:foreground "chartreuse"))))
   '(rainbow-delimiters-depth-4-face ((t (:foreground "deep sky blue"))))
   '(rainbow-delimiters-depth-5-face ((t (:foreground "yellow"))))
   '(rainbow-delimiters-depth-6-face ((t (:foreground "orchid"))))
   '(rainbow-delimiters-depth-7-face ((t (:foreground "spring green"))))
   '(rainbow-delimiters-depth-8-face ((t (:foreground "sienna1")))))
  )
