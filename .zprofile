# Environment Variables
#set EDITOR=vim
#set RANGER_LOAD_DEFAULT_RC=FALSE
#export QT_STYLE_OVERRIDE="ukui-dark"
export LOCATION="31023"
export VISUAL=vim
export EDITOR="$VISUAL"
export GEM_HOME=$HOME/.gem
export KWIN_DRM_USE_EGL_STREAMS=1
export PATH="$PATH:$(ruby -e 'print Gem.user_dir')/bin"
export PATH="$PATH:/home/jrobc2e/.local/bin/"
export PATH=/home/jrobc2e/.emacs.d/bin:$PATH
export PATH=/home/jrobc2e/.local/bin/lukeScripts/.local/bin/statusbar:$PATH
eval $(ssh-agent)
if [ -n "$DESKTOP_SESSION" ];then
	    eval $(gnome-keyring-daemon --start)
	        export SSH_AUTH_SOCK
fi
# exec /home/jrobc2e/.scripts/wm-autostart.sh
# if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
# 	exec startx
# fi
