;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "John Doe"
      user-mail-address "john@doe.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
(setq doom-font (font-spec :family "IosevkaBest" :size 14))
;; (setq doom-variable-pitch-font (font-spec :family "IBM Plex Sans" :size 14))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
;; (setq doom-theme 'doom-one)
;; (setq doom-theme 'doom-acario-dark)
;; (setq doom-acario-dark-brighter-modeline t)
(setq doom-theme 'doom-outrun-electric)
;; (setq doom-theme 'doom-oceanic-next)
;; (setq doom-themes-enable-italic nil)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "/home/jrobc2e/Dropbox/Org/")
(setq org-agenda-files '("/home/jrobc2e/Dropbox/Org/Agenda.org" "/home/jrobc2e/Dropbox/Org/Notes.org"))
(setq org-agenda-contributing-files '("/home/jrobc2e/Dropbox/Org/Agenda.org" "/home/jrobc2e/Dropbox/Org/Notes.org"))
(setq deft-directory "/home/jrobc2e/Dropbox/Org/roam")
(setq org-roam-directory "/home/jrobc2e/Dropbox/Org/roam")
(setq deft-recursive t)
;; (setq org-journal-dir "/home/jrobc2e/Dropbox/Org/journal")
;; (setq org-journal-file-type 'yearly
;;       org-journal-time-format ""
;;       ;; org-journal-time-prefix ""
;;       )

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)
;; (setq display-line-numbers-type 'relative)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c g k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c g d') to jump to their definition and see how
;; they are implemented.

;- UI
(setq evil-goggles-pulse t)
(setq evil-goggles-duration 0.15)
;; (setq evil-goggles-enable-change t)
;; (setq evil-goggles-enable-delete t)
;; (evil-goggles-use-diff-refine-faces)

;- org-mode
(use-package! org-download
  :config
  (setq org-download-screenshot-method "xclip -selection clipboard -t image/png -o > %s"))
  (setq org-catch-invisible-edits t
  org-download-image-dir "/home/jrobc2e/Dropbox/School/.org/.orgDownloads/"
  ;; org-download-screenshot-method "xclip -selection clipboard -t image/png -o > %s"
  org-download-annotate-function (lambda (_) "")
  org-image-actual-width nil
  org-latex-image-default-width ""
  org-pretty-entities t
  org-pretty-entities-include-sub-superscripts t
  org-use-sub-superscripts "{}"
  org-hide-emphasis-markers t
  ;; (setq org-inline-image-overlays t)
  org-superstar-headline-bullets-list '("■" "◆" "▲" "▶")
  org-ellipsis " ▼"
  org-fontify-whole-heading-line t
  org-fontify-done-headline t
  org-fontify-emphasized-text t
  org-fontify-quote-and-verse-blocks t
  org-hide-leading-stars t)

(after! org
  (remove-hook 'org-tab-first-hook #'+org-cycle-only-current-subtree-h) ; Preserve normal <tab> behavior
  ;; (setq org-pomodoro-manual-break t) ; require initiating transitions manually
  (setq org-capture-templates
        '(("p" "program")
          ("pe" "emacs" item (file+headline "/home/jrobc2e/Dropbox/Org/Notes.org" "Doom Emacs")
           "- %?\n" :prepend t :kill-buffer t)
          ("pd" "doublecmd" item (file+headline "/home/jrobc2e/Dropbox/Org/Notes.org" "Double commander")
           "- %?\n" :prepend t :kill-buffer t)
          ("pk" "kwin" item (file+headline "/home/jrobc2e/Dropbox/Org/Notes.org" "Kwin")
           "- %?\n" :prepend t :kill-buffer t)
          ("pf" "firefox" item (file+headline "/home/jrobc2e/Dropbox/Org/Notes.org" "Firefox")
           "- %?\n" :prepend t :kill-buffer t)
          ("pa" "anki" item (file+headline "/home/jrobc2e/Dropbox/Org/Notes.org" "Anki")
           "- %?\n" :prepend t :kill-buffer t)
          ("a" "agenda entry" entry (file "/home/jrobc2e/Dropbox/Org/Agenda.org")
           "* %?\n" :prepend t :kill-buffer t)
          ("t" "todo entry" entry (file "/home/jrobc2e/Dropbox/Org/Agenda.org")
           "* TODO %?\n" :prepend t :kill-buffer t)
          ;; "* TODO %?\n :PROPERTIES:\n :CATEGORY: dream\n :END:\n %i\n" :prepend t :kill-buffer t))
          org-todo-keywords '((sequence "TODO(t)" "NEXT(n)" "PROJ(p)" "MYBE(m)" "WAIT(w)" "|" "DONE(d)"))
          org-todo-keyword-faces '(("NEXT" . +org-todo-active)
                                   ("WAIT" . +org-todo-onhold)
                                   ("PROJ" . +org-todo-project)
                                   ("MYBE" . +org-todo-onhold))
          )))

(setq org-clock-mode-line-total 'today)

;- interaction
(setq evil-snipe-scope 'visible)

; My functions
(defun myscreenshot (arg)
  (interactive "P")
  (beginning-of-line)
  (let ((beg (point))))
  (org-download-screenshot)
  (forward-line -2)
  (kill-line)
  (when arg
    (mySizeLabel arg))
  ;; (open-line 1)
  )
(defun my-ex-replacement (&optional arg)
  (interactive "P")
  (let ((evil-ex-substitute-global t))
    (cond ;; ((eq arg 2) (evil-ex "'<,'>s/\\, /,\\n/"))
     ;; ((eq arg 4) (evil-ex "'<,'>s/^[[:space:]]*\\(•\\|\\|\\|◼\\)\\n*[[:space:]]*/- /"))
     ;; ((eq arg 6) (evil-ex "'<,'>s/“\\|”/\"/"))
     ;; ((eq arg 7) (evil-ex "'<,'>s/‘\\|’/'/"))
     ((eq arg 1) (evil-ex "'<,'>s/^[[:space:]]*\\(•\\|\\|\\|◼\\)\\n*[[:space:]]*/- /")
      (evil-ex "'<,'>s/“\\|”/\"/") (evil-ex "'<,'>s/‘\\|’/'/")) ; get rid of bad symbols
     ((eq arg 2) (evil-ex "'<,'>s/^[[:space:]]*\\n//"))
     ((eq arg 3) (evil-ex "'<,'>s/-?\\n\\([^-] *\\)/ \\1")) ;; try to unite hyphenated words over newlines
     ;; without count, delete empty space and optionally split line on punctuation points
     (t (evil-ex "'<,'>s/^[[:space:]]*\\n//") (evil-ex "'<,'>s/\\(\\.\\|?\\|!\\) /\\1\\n/") (evil-ex "'<,'>s/\\, /,\\n/")))
    ))
(defun myExp (arg)
  (interactive "p")
  (insert (format "\\times10^{%d}" arg))
  )
(defun mySizeLabel (arg)
  (interactive "P")
  (beginning-of-line)
  (if (> arg 20)
      (progn
        (insert (format "#+ATTR_ORG: :width %spx\n" arg))
        (insert (format "#+ATTR_LATEX: :width %spx\n" (round (* arg 0.64))))
        (org-display-inline-images))
    (progn
      (insert (format "#+ATTR_ORG: :scale %.2f\n" (/ 1.0 arg)))
      (insert (format "#+ATTR_LATEX: :scale %.2f\n" (/ 1.0 arg)))
      (org-display-inline-images))
    )
  )

;; erc
;; (setq erc- )

;- keybinds
(after! evil
  (map! (:prefix "]"
         :desc "Next window" :n "w" #'evil-window-next
         :desc "Next workspace" :n "g" #'+workspace/switch-right)
        (:prefix "["
         :desc "Prev window" :n "w" #'evil-window-prev
         :desc "Prev workspace" :n "g" #'+workspace/switch-left)
        (:leader
         :desc "Next window" :n "]" #'evil-window-next
         :desc "Prev window" :n "[" #'evil-window-prev
         :desc "Next workspace" :n "}" #'+workspace/switch-right
         :desc "Prev workspace" :n "{" #'+workspace/switch-left
         ))
  )

(map! (:desc "Avy with timer" :n "C-;" #'evil-avy-goto-char-timer
       :desc "Avy with timer" :n "C-:" #'vimish-fold-avy)
      (:leader :prefix "d"
               ;; :desc "Insert clipboard image" :n "s" #'org-download-screenshot
               :desc "Toggle pretty symbols" :n "p" #'+org-pretty-mode
               :desc "Toggle link view" :n "P" #'org-toggle-link-display)
      (:prefix "g" :desc "Bump down (copy+comment) text" :nv "b" #'evilnc-copy-and-comment-lines)
      (:leader :desc "Open scratch buffer" :nv "X" #'doom/open-scratch-buffer)
      (:leader :desc "Org capture" :nv "x" #'org-capture)
      )

(map! :leader
      :desc "Replace using evil-ex" :n "r" #'my-ex-replacement
      :prefix "d" :desc "my screenshot" :n "s" #'myscreenshot
      :prefix "d" :desc "my screenshot" :n "S" #'mySizeLabel
      )

(setq evil-escape-key-sequence "fd")

;; Put folding indicator on left side
(setq vimish-fold-indication-mode 'left-fringe)

;; (defun +workspaces-restore-last-session (&rest _)
;;   (select-frame-set-input-focus (selected-frame))
;;   (add-hook 'emacs-startup-hook #'+workspace/load-session :append))

;;   Desktop persistence (not working for some reason)
;; (add-hook 'window-setup-hook #'doom/quickload-session)

(display-time)
;; (after! )
(setq display-time-24hr-format t)
(setq display-time-default-load-average 1)

;- faces
;; First lines fix bad comment font family used by some themes
(custom-set-faces!
  '(font-lock-comment-face :family "IosevkaBest")
  '(org-level-1 :inherit outline-1 :height 1.2)
  '(org-level-2 :inherit outline-2 :height 1.1)
  '(org-level-3 :inherit outline-3 :height 1.1)
  ;; '(font-lock-keyword-face :family "IosevkaBest")
  ;; '(evil-goggles-delete-face :inherit 'shadow)
  ;; '(evil-goggles-paste-face :inherit 'lazy-highlight)
  ;; '(evil-goggles-yank-face :inherit 'isearch-fail)
  )

;; get rid of extraneous "total" line and title line in clocktables
(defun my-clocktable (ipos tables params)
  "Write out a clock table at position IPOS in the current buffer.
TABLES is a list of tables with clocking data as produced by
`org-clock-get-table-data'.  PARAMS is the parameter property list obtained
from the dynamic block definition."
  ;; This function looks quite complicated, mainly because there are a
  ;; lot of options which can add or remove columns.  I have massively
  ;; commented this function, the I hope it is understandable.  If
  ;; someone wants to write their own special formatter, this maybe
  ;; much easier because there can be a fixed format with a
  ;; well-defined number of columns...
  (let* ((lang (or (plist-get params :lang) "en"))
         (multifile (plist-get params :multifile))
         (block (plist-get params :block))
         (sort (plist-get params :sort))
         (header (plist-get params :header))
         (link (plist-get params :link))
         (maxlevel (or (plist-get params :maxlevel) 3))
         (emph (plist-get params :emphasize))
         (compact? (plist-get params :compact))
         (narrow (or (plist-get params :narrow) (and compact? '40!)))
         (level? (and (not compact?) (plist-get params :level)))
         (timestamp (plist-get params :timestamp))
         (tags (plist-get params :tags))
         (properties (plist-get params :properties))
         (time-columns
          (if (or compact? (< maxlevel 2)) 1
            ;; Deepest headline level is a hard limit for the number
            ;; of time columns.
            (let ((levels
                   (cl-mapcan
                    (lambda (table)
                      (pcase table
                        (`(,_ ,(and (pred wholenump) (pred (/= 0))) ,entries)
                         (mapcar #'car entries))))
                    tables)))
              (min maxlevel
                   (or (plist-get params :tcolumns) 100)
                   (if (null levels) 1 (apply #'max levels))))))
         (indent (or compact? (plist-get params :indent)))
         (formula (plist-get params :formula))
         (case-fold-search t)
         (total-time (apply #'+ (mapcar #'cadr tables)))
         recalc narrow-cut-p)

    (when (and narrow (integerp narrow) link)
      ;; We cannot have both integer narrow and link.
      (message "Using hard narrowing in clocktable to allow for links")
      (setq narrow (intern (format "%d!" narrow))))

    (pcase narrow
      ((or `nil (pred integerp)) nil)	;nothing to do
      ((and (pred symbolp)
            (guard (string-match-p "\\`[0-9]+!\\'" (symbol-name narrow))))
       (setq narrow-cut-p t)
       (setq narrow (string-to-number (symbol-name narrow))))
      (_ (user-error "Invalid value %s of :narrow property in clock table" narrow)))

    ;; Now we need to output this table stuff.
    (goto-char ipos)

    ;; Insert the text *before* the actual table.
    (insert-before-markers
     (or header
         ;; Format the standard header.
         (format "#+CAPTION: %s %s%s\n"
                 (org-clock--translate "Clock summary at" lang)
                 (format-time-string (org-time-stamp-format t t))
                 (if block
                     (let ((range-text
                            (nth 2 (org-clock-special-range
                                    block nil t
                                    (plist-get params :wstart)
                                    (plist-get params :mstart)))))
                       (format ", for %s." range-text))
                   ""))))

    ;; Insert the narrowing line
    (when (and narrow (integerp narrow) (not narrow-cut-p))
      (insert-before-markers
       "|"				;table line starter
       (if multifile "|" "")		;file column, maybe
       (if level? "|" "")		;level column, maybe
       (if timestamp "|" "")		;timestamp column, maybe
       (if tags "|" "")                 ;tags columns, maybe
       (if properties			;properties columns, maybe
           (make-string (length properties) ?|)
         "")
       (format "<%d>| |\n" narrow)))	;headline and time columns

    ;; ;; Insert the table header line
    ;; (insert-before-markers
    ;;  "|"				;table line starter
    ;;  (if multifile			;file column, maybe
    ;;      (concat (org-clock--translate "File" lang) "|")
    ;;    "")
    ;;  (if level?				;level column, maybe
    ;;      (concat (org-clock--translate "L" lang) "|")
    ;;    "")
    ;;  (if timestamp			;timestamp column, maybe
    ;;      (concat (org-clock--translate "Timestamp" lang) "|")
    ;;    "")
    ;;  (if tags "Tags |" "")              ;tags columns, maybe

    ;;  (if properties			;properties columns, maybe
    ;;      (concat (mapconcat #'identity properties "|") "|")
    ;;    "")
    ;;  (concat (org-clock--translate "Headline" lang)"|")
    ;;  (concat (org-clock--translate "Time" lang) "|")
    ;;  (make-string (max 0 (1- time-columns)) ?|) ;other time columns
    ;;  (if (eq formula '%) "%|\n" "\n"))

    ;; ;; Insert the total time in the table
    ;; (insert-before-markers
    ;;  "|-\n"				;a hline
    ;;  "|"				;table line starter
    ;;  (if multifile (format "| %s " (org-clock--translate "ALL" lang)) "")
    ;; 			;file column, maybe
    ;;  (if level?    "|" "")		;level column, maybe
    ;;  (if timestamp "|" "")		;timestamp column, maybe
    ;;  (if tags      "|" "")		;timestamp column, maybe
    ;;  (make-string (length properties) ?|) ;properties columns, maybe
    ;;  (concat (format org-clock-total-time-cell-format
    ;;      (org-clock--translate "Total time" lang))
    ;;    "| ")
    ;;  (format org-clock-total-time-cell-format
    ;;    (org-duration-from-minutes (or total-time 0))) ;time
    ;;  "|"
    ;;  (make-string (max 0 (1- time-columns)) ?|)
    ;;  (cond ((not (eq formula '%)) "")
    ;;  ((or (not total-time) (= total-time 0)) "0.0|")
    ;;  (t  "100.0|"))
    ;;  "\n")

    ;; Now iterate over the tables and insert the data but only if any
    ;; time has been collected.
    (when (and total-time (> total-time 0))
      (pcase-dolist (`(,file-name ,file-time ,entries) tables)
        (when (or (and file-time (> file-time 0))
                  (not (plist-get params :fileskip0)))
          (insert-before-markers "|-\n") ;hline at new file
          ;; First the file time, if we have multiple files.
          (when multifile
            ;; Summarize the time collected from this file.
            (insert-before-markers
             (format (concat "| %s %s | %s%s%s"
                             (format org-clock-file-time-cell-format
                                     (org-clock--translate "File time" lang))
                             " | *%s*|\n")
                     (file-name-nondirectory file-name)
                     (if level?    "| " "") ;level column, maybe
                     (if timestamp "| " "") ;timestamp column, maybe
                     (if tags      "| " "") ;tags column, maybe
                     (if properties	    ;properties columns, maybe
                         (make-string (length properties) ?|)
                       "")
                     (org-duration-from-minutes file-time)))) ;time

          ;; Get the list of node entries and iterate over it
          (when (> maxlevel 0)
            (pcase-dolist (`(,level ,headline ,tgs ,ts ,time ,props) entries)
              (when narrow-cut-p
                (setq headline
                      (if (and (string-match
                                (format "\\`%s\\'" org-link-bracket-re)
                                headline)
                               (match-end 2))
                          (format "[[%s][%s]]"
                                  (match-string 1 headline)
                                  (org-shorten-string (match-string 2 headline)
                                                      narrow))
                        (org-shorten-string headline narrow))))
              (cl-flet ((format-field (f) (format (cond ((not emph) "%s |")
                                                        ((= level 1) "*%s* |")
                                                        ((= level 2) "/%s/ |")
                                                        (t "%s |"))
                                                  f)))
                (insert-before-markers
                 "|"		       ;start the table line
                 (if multifile "|" "") ;free space for file name column?
                 (if level? (format "%d|" level) "") ;level, maybe
                 (if timestamp (concat ts "|") "")   ;timestamp, maybe
                 (if tags (concat (mapconcat #'identity tgs ", ") "|") "")   ;tags, maybe
                 (if properties		;properties columns, maybe
                     (concat (mapconcat (lambda (p) (or (cdr (assoc p props)) ""))
                                        properties
                                        "|")
                             "|")
                   "")
                 (if indent		;indentation
                     (org-clocktable-indent-string level)
                   "")
                 (format-field headline)
                 ;; Empty fields for higher levels.
                 (make-string (max 0 (1- (min time-columns level))) ?|)
                 (format-field (org-duration-from-minutes time))
                 (make-string (max 0 (- time-columns level)) ?|)
                 (if (eq formula '%)
                     (format "%.1f |" (* 100 (/ time (float total-time))))
                   "")
                 "\n")))))))
    (delete-char -1)
    (cond
     ;; Possibly rescue old formula?
     ((or (not formula) (eq formula '%))
      (let ((contents (org-string-nw-p (plist-get params :content))))
        (when (and contents (string-match "^\\([ \t]*#\\+tblfm:.*\\)" contents))
          (setq recalc t)
          (insert "\n" (match-string 1 contents))
          (beginning-of-line 0))))
     ;; Insert specified formula line.
     ((stringp formula)
      (insert "\n#+TBLFM: " formula)
      (setq recalc t))
     (t
      (user-error "Invalid :formula parameter in clocktable")))
    ;; Back to beginning, align the table, recalculate if necessary.
    (goto-char ipos)
    (skip-chars-forward "^|")
    (org-table-align)
    (when org-hide-emphasis-markers
      ;; We need to align a second time.
      (org-table-align))
    (when sort
      (save-excursion
        (org-table-goto-line 3)
        (org-table-goto-column (car sort))
        (org-table-sort-lines nil (cdr sort))))
    (when recalc (org-table-recalculate 'all))
    total-time))

(setq org-clock-clocktable-formatter #'my-clocktable)
