" vim-plug
call plug#begin('~/.vim/plug')
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-vividchalk'
Plug 'vim-scripts/sudo.vim'
Plug 'lervag/vimtex'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'tomasr/molokai'
Plug 'mhartington/oceanic-next'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'bling/vim-bufferline'
Plug 'ap/vim-css-color'
Plug 'chrisbra/Colorizer'
Plug 'rakr/vim-one'
call plug#end()

inoremap fd <Esc>
let g:vimtex_view_method = 'zathura'

set nocompatible
set encoding=UTF-8
set clipboard+=unnamedplus
"set paste
syntax on
set wildmenu
set showcmd

" searching only case sensitive if capital letters were used
set ignorecase
set smartcase
set number
set relativenumber
" Dark numbers
" highlight LineNr ctermfg=DarkGrey
" highlight CursorLineNr ctermfg=Grey ctermbg=Black

" Nord Scheme
"let g:nord_cursor_line_number_background = 1
"colorscheme nord

set background=dark
colorscheme one
"colorscheme solarized
"let g:airline_theme='behelit'
let g:airline_theme='onedark'
let g:airline_powerline_fonts = 1
"let g:airline#extensions#tabline#enabled = 1
"let g:ar

let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

set cursorline
"hi cursorline cterm=underline term=underline
highlight CursorLine guibg=bg
