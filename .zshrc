setopt glob_star_short
# unsetopt menu_complete
setopt auto_menu
setopt hist_ignore_dups share_history inc_append_history extended_history
setopt nohistverify # don't expand history completion on first enter; just run command, with first enter
#setopt extended_glob

# allow skipping history by starting line with a space
setopt HIST_IGNORE_SPACE

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _ignored
zstyle ':completion:*' completions 1
zstyle ':completion:*' glob 1
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt '%SAt %p: Hit TAB for more, or the character to insert%s'
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt '%SScrolling active: current selection at %p%s'
zstyle ':completion:*' substitute 1
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/jrobc2e/.zshrc'

autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit

_comp_options+=(globdots) # Include hidden files.

HISTFILE=~/.histfile
HISTSIZE=500000
SAVEHIST=500000
setopt autocd
unsetopt beep extendedglob nomatch

## Vi-mode
bindkey -v
export KEYTIMEOUT=1
bindkey -M viins 'fd' vi-cmd-mode
## Xcape: tap Caps for ESC
#xcape -t 300 -e 'Control_L=Escape'

# Use vim keys in tab complete menu:
bindkey -M menuselect '^[h' vi-backward-char
bindkey -M menuselect '^[k' vi-up-line-or-history
bindkey -M menuselect '^[l' vi-forward-char
bindkey -M menuselect '^[j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

zle -N zle-line-init
zle -N zle-keymap-select

## System Configuration
# yakuakeblur (zsh)
#if [[ $(ps --no-header -p $PPID -o comm) =~ '^yakuake|konsole$' ]]; then
#        for wid in $(xdotool search --pid $PPID); do
#            xprop -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 -id $wid; done
#fi

## Aliases & functions
alias xmm='/home/jrobc2e/.local/bin/xmodmap.sh'
alias fullclean='make clean && rm -f config.h && git reset --hard origin/master'
alias cfg='/usr/bin/git --git-dir=/home/jrobc2e/.cfg/ --work-tree=/home/jrobc2e' # use in place of "git" for dotfile sync
alias vf='vifmrun'
alias hy='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia vblank_mode=0'
alias aps='wine /home/jrobc2e/.wine/drive_c/Program\ Files\ \(x86\)/Adobe/Adobe\ Photoshop\ CC\ 2018\ \(32\ Bit\)/Photoshop.exe --with-opencl'
alias v='f -e vim'   # quick opening files with vim
alias e='f -e "emacsclient"' # quick opening files with emacs
alias en='f -e "emacsclient -c"' # quick opening files with emacs
alias ra='ranger'
disable r # r is a builtin that repeats the last command
alias r='d -e ranger'
alias ls='ls --color=always'
alias dt='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia vblank_mode=0 darktable'

spc() {
	df -h | grep "sda5\|sda4" | awk '{print $4, $5}'
}

fa() {
	ffmpeg -i $1 -c:a pcm_s16le $2
}

fv() {
	ffmpeg -i $1 -c:v dnxhd -profile:v dnxhr_lb -c:a pcm_s16le $2
}

fvs() {
	ffmpeg -i $1 -c:v mpeg4 -force_key_frames c:a pcm_s16le $2
}

ft() {
	ffmpeg -ss $1 -t $2 -i $3 -avoid_negative_ts make_zero -c copy $4
}

fto() {
	ffmpeg -ss $1 -to $2 -i $3 -avoid_negative_ts make_zero -c copy $4
}

fw() {
#	ffmpeg -loglevel quiet -stats -i $1 -c copy -bsf:a aac_adtstoasc /home/jrobc2e/Dropbox/School/Ecol/Videos/$2.mp4
	ffmpeg -loglevel quiet -stats -i $1 -c copy /home/jrobc2e/drive/__MUSI-2700/$2.mp4
}

## firefox accel
export MOZ_USE_OMTC=1

# Enable Ctrl-x-e to edit command line
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^xe' edit-command-line
bindkey '^x^e' edit-command-line

# Set vim as default editor
export EDITOR=vim

## colored manpages through customization of less
export LESS=-R
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

# startup fortune - no preference
# fortune -a

# LS_COLORS
eval $(dircolors -b /home/jrobc2e/.dircolors-onedark)
#eval $(dircolors -b /home/jrobc2e/.dircolors_solarized)
#export LS_COLORS="$(vivid generate snazzy)"

# SOURCING
#source ~/.zsh-syntax-highlighting-filetypes
#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
# zle -N up-line-or-beginning-search
# zle -N down-line-or-beginning-search

# [[ -n "$key[Up]"   ]] && bindkey -- "^[[A" up-line-or-beginning-search
# [[ -n "$key[Down]" ]] && bindkey -- "^[[B" down-line-or-beginning-search

# bindkey "^[p" up-line-or-beginning-search # Up
# bindkey "^[n" down-line-or-beginning-search # Down

bindkey '^P' up-history
bindkey '^N' down-history
bindkey '^?' backward-delete-char
bindkey '^h' backward-delete-char
bindkey '^w' backward-kill-word
bindkey '^r' history-incremental-search-backward
bindkey '^[[3~' delete-char # fix Delete key's function
bindkey -M vicmd '^[[3~' delete-char # fix Delete key's function in normal mode
bindkey '^]' forward-word
bindkey '^\' end-of-line

backward-kill-dir () {
    local WORDCHARS="${WORDCHARS:s@/@}"
    zle backward-kill-word
}

zle -N backward-kill-dir
zle -N backward-kill-dir
#bindkey '^[^?' backward-kill-dir
bindkey '^[^?' backward-kill-dir
bindkey '^W' backward-kill-word

bindkey -M viins "^[0" digit-argument
# bindkey -M viins "^[1" digit-argument
# bindkey -M viins "^[2" digit-argument
# bindkey -M viins "^[3" digit-argument
# bindkey -M viins "^[4" digit-argument
# bindkey -M viins "^[5" digit-argument
# bindkey -M viins "^[6" digit-argument
# bindkey -M viins "^[7" digit-argument
# bindkey -M viins "^[8" digit-argument
# bindkey -M viins "^[9" digit-argument

# Add slash to respected delimiters for words
#export WORDCHARS=${WORDCHARS//[\/]}

## Prompts
# #PROMPT='%B%F{242}[%f%b%F{12}%20<..<%3~%f%B%F{242}]%f%b%F{220}>%f '
# PROMPT='%B%F{240}[%f%b%F{33}%20<..<%3~%f%B%F{240}]%f%b%B%F{252}>%f%b '
# RPROMPT='%F{160}%*%f'
# For better compatibility with wal
PROMPT='%B%F{240}[%f%b%F{blue}%20<..<%3~%f%B%F{240}]%f%b%B%F{252}>%f%b '
RPROMPT='%F{red}%*%f'

function zle-keymap-select zle-line-init
{
    case $KEYMAP in
        vicmd)      print -n -- "\e[1 q";;  # block cursor
        viins|main) print -n -- "\e[5 q";;  # line cursor
    esac

    zle reset-prompt
    zle -R
}

function zle-line-finish
{
    print -n -- "\e[1 q"  # block cursor
}

zle -N zle-line-init
zle -N zle-line-finish
zle -N zle-keymap-select

# fzf-extras
[[ -e "/usr/share/fzf/fzf-extras.zsh" ]] \
	&& source /usr/share/fzf/fzf-extras.zsh

# _fasd_preexec() {
#     { eval "fasd --proc \$(fasd --sanitize \$2)"; } &|
# }
# autoload -Uz add-zsh-hook
# add-zsh-hook preexec _fasd_preexec

## fasd setup
# eval "$(fasd --init posix-alias zsh-hook)"
# eval "$(fasd --init posix-alias zsh-hook zsh-ccomp zsh-ccomp-install zsh-wcomp zsh-wcomp-install)"
eval "$(fasd --init auto)"
bindkey '^X^A' fasd-complete    # C-x C-a to do fasd-complete (files and directories)
bindkey '^X^F' fasd-complete-f  # C-x C-f to do fasd-complete-f (only files)
bindkey '^X^D' fasd-complete-d  # C-x C-d to do fasd-complete-d (only directories)
# fasd_cache="$HOME/.fasd-init-zsh"
# if [ "$(command -v fasd)" -nt "$fasd_cache" -o ! -s "$fasd_cache" ]; then
# 	fasd --init posix-alias zsh-hook zsh-ccomp zsh-ccomp-install >| "$fasd_cache"
# fi
# source "$fasd_cache"
# unset fasd_cache

# source <(antibody init)
# antibody bundle < /home/jrobc2e/.zsh_plugins.txt
# export FZFZ_RECENT_DIRS_TOOL='fasd'

# nnn
export TERMINAL="${TERMINAL:-konsole}"
export NNN_PLUG='f:finder;o:fzopen;p:mocplay;d:diffs;n:nmount;v:imgview;m:-mediainf;w:-wall;r:renamer;t:treeview;a:preview-tui;k:kdeconnect'
export NNN_OPENER=/home/jrobc2e/.config/nnn/plugins/nuke
export NNN_FIFO=/tmp/nnn.fifo

# fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh
export FZF_DEFAULT_OPTS='--height 90% --layout=reverse --border'
export FZF_DEFAULT_COMMAND='rg --files --no-ignore-vcs --hidden'
# export FZF_ALT_C_OPTS='' FORGET ABOUT IT; TAKES WAY TOO LONG TO INDEX ALL HIDDEN DIRS
# alias v='vim $(fzf)'   # fzf opening files with vim (no recency score; makes less sense)
# alias e='emacsclient -c $(fzf)' # fzf opening files with emacs (no recency score; makes less sense)

# Using highlight (http://www.andre-simon.de/doku/highlight/en/highlight.html)
export FZF_CTRL_T_OPTS="--preview '(highlight -O ansi -l {} 2> /dev/null || cat {} || tree -C {}) 2> /dev/null | head -200'"

# Plugins
#plugins=(git ssh-agent)

# zsh-autosuggestions
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

# substring search: should be AFTER syntax highlighting
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh 2>/dev/null
bindkey "^[p" history-substring-search-up # Up
bindkey "^[n" history-substring-search-down # Down

autoload copy-earlier-word
zle -N copy-earlier-word
bindkey -M viins '^[,' copy-earlier-word
